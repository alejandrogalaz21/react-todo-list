import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import List from './components/list/List'
import Form from './components/form/Form'
class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      type: {},
      todos:[]      
    }
  }

  onChange = type => {
    this.setState({
      type: {...this.state.type, ...type}
    })
  }

  onSubmit = (todo) => {
    
    this.setState({
      todos:[...this.state.todos, todo]
    })

  }

  deleteAllItems = () => {
   
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">My todo list</h1>
        </header>
        <br/>
        <Form 
          bubleUp={this.onSubmit}
          onChange={this.onChange} 
        />
        <pre>
          {JSON.stringify(this.state.type, null)}
        </pre>
        <div>
          <List todos={this.state.todos}/>
        </div>
      </div>
    );
  }
}

export default App;
