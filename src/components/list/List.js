import React from 'react'

const List = props => (
  <ul>
    {
       props.todos.map((item, index) => <li key={index}> 
          <h1>{item.name}</h1>
          <p>{item.desc}</p>
        </li> )
    }
  </ul>  
)

export default List