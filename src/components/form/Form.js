import React, {Component} from 'react'

export default class Form extends Component {

  constructor(props) {
    super(props)
    this.state = {
      name:'',
      desc:''
    }
  }

  onChange = e => {
    this.props.onChange({[e.target.name]: e.target.value})
    this.setState({[e.target.name]: e.target.value})
    
  }

  onSubmit = e => {
    e.preventDefault()
    this.props.bubleUp(this.state)
    this.setState({ 
      name:'',
      desc:''
    })
    this.props.onChange({ 
      name:'',
      desc:''
    })
  }

  render() {
    return (
      <form className="form" onSubmit={e => this.onSubmit(e)}>
          <input name="name" value={this.state.name} onChange={this.onChange} />
          <input name="desc" value={this.state.desc} onChange={this.onChange} />
          <button>add todo</button>
          <button onClick={this.deleteAllItems}>
            delete all 
          </button>
        </form>
    )
  }
}